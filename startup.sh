#!/bin/bash

# Build docker image
echo "Building images"
docker-compose up -d --build
# Run all the services except the consumers, which will be run after all the apps are prepaired
echo "Running up images with out consumers"
docker-compose up -d --scale consumers=0

######################################################################################
# Prepare To-Do Django container
######################################################################################
echo "Preparing to Django?"
docker-compose exec todo git checkout master
echo "Running migration scripts?"
"scripts/migrateToDo.sh"

# after all set and done run the consumers
echo "Running up consumers"
docker-compose up -d consumers
# start the cron jobs and set the commands under todo app
echo "Starting up todo cron"
docker-compose exec todo service cron start
echo "Starting run crontab?"
docker-compose exec todo crontab ../../../etc/crontab
